# soal-shift-sisop-modul-3-a04-2022 

|     NRP    |     Nama    |
| :--------- |:------------    |
| 5025201098 | Ibra Abdi Ibadihi
| 5025201184 | Cahyadi Surya Nugraha |
| 5025201007 | Sejati Bakti Raga

# Soal 1
## Catatan
- Boleh menggunakan execv(), tapi bukan mkdir, atau system
- Jika ada pembenaran soal, akan di-update di catatan
- Kedua file .zip berada di folder modul
- Nama user di passwordnya adalah nama salah satu anggota kelompok, baik yang mengerjakan soal tersebut atau tidak
- Misalkan satu kelompok memiliki anggota namanya Yudhistira, Werkudara, dan Janaka. Yang mengerjakan adalah Janaka. Nama passwordnya bisa mihinomenestyudhistira, mihinomenestwerkudara, atau mihinomenestjanaka
- Menggunakan apa yang sudah dipelajari di Modul 3 dan, kalau  perlu, di Modul 1 dan 2

## A. Download dan Unzip File
Proses download dan unzip file yang disediakan dilakukan dengan fungsi download_file. Untuk pendownloadan digunakan fungsi execv yang menjalankan fungsi wget dengan argumen url file yang telah diberikan pada soal. pthread_equal digunakan untuk membandingkan thread ID dan menjalankannya pada waktu yang sama sesuai permintaan soal. 

```
#define MUSIC_URL "https://drive.google.com/u/0/uc?id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1&export=download"
#define QUOTE_URL "https://drive.google.com/u/0/uc?id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt&export=download"
#define PATH "~/Documents/praktikum-sisop/praktikum-3/soal1"
#define MUSIC "music.zip"
#define QUOTE "quote.zip"
#define HASIL "hasil.zip"

void *download_file(void *arg)
{
  pthread_t id = pthread_self();
  if (pthread_equal(id, tid[0]))
  {
    int status;
    if (fork() == 0)
    {
      char *argv[] = {"mkdir", "-p", "music", NULL};
      execv("/usr/bin/mkdir", argv);
    }
    else
    {
      while ((wait(&status)) > 0)
        ;
      int status2;

      if (fork() == 0)
      {
        char *argv[] = {"wget", "-q", "--no-check-certificate", MUSIC_URL, "-O", MUSIC, NULL};
        execv("/usr/bin/wget", argv);
      }

      while ((wait(&status2)) > 0)
        ;

      if (fork() == 0)
      {
        char *argv[] = {"unzip", "-qq", MUSIC, "-d", "music", NULL};
        execv("/usr/bin/unzip", argv);
      }

      while ((wait(&status2)) > 0)
        ;
    }
  }
  else if (pthread_equal(id, tid[1]))
  {
    pthread_join(tid[0], NULL);

    int status;
    if (fork() == 0)
    {
      char *argv[] = {"mkdir", "-p", "quote", NULL};
      execv("/usr/bin/mkdir", argv);
    }
    else
    {
      while ((wait(&status)) > 0)
        ;
      int status2;
      if (fork() == 0)
      {
        char *argv[] = {"wget", "-q", "--no-check-certificate", QUOTE_URL, "-O", QUOTE, NULL};
        execv("/usr/bin/wget", argv);
      }

      while ((wait(&status2)) > 0)
        ;

      if (fork() == 0)
      {
        char *argv[] = {"unzip", "-qq", QUOTE, "-d", "quote", NULL};
        execv("/usr/bin/unzip", argv);
      }

      while ((wait(&status2)) > 0)
        ;

      if (fork() == 0)
      {
        char *argv[] = {"mkdir", "-p", "hasil", NULL};
        execv("/usr/bin/mkdir", argv);
      }

      while ((wait(&status2)) > 0)
        ;
    }
  }
}
```

## B. Decode File
Di dalam soal kita diminta untuk melakukan decode file .txt dengan base 64. Pertama, kita buat tabel untuk decoding.

```
char base64[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'};
char *decoding_table;
```
Decoding file, dilakukan menggunakan fungsi decoding.

```
void decoding(char *txt)
{
  char buff[4], result[100];
  int i = 0, index = 0, count = 0, j;

  while (txt[i] != '\0')
  {
    for (j = 0; j < 64 && base64[j] != txt[i]; j++)
      ;
    buff[count++] = j;
    if (count == 4)
    {
      result[index++] = (buff[0] << 2) + (buff[1] >> 4);
      if (buff[2] != 64)
        result[index++] = (buff[1] << 4) + (buff[2] >> 2);
      if (buff[3] != 64)
        result[index++] = (buff[2] << 6) + buff[3];
      count = 0;
    }
    i++;
  }

  result[index] = '\0';
  strcpy(res, result);
}
```
Proses decoding dilakukan dengan menjalankan fungsi decode yang akan memanggil fungsi decoding. Fungsi decode akan menjalankan dua thread yang masing-masing akan membuka file quote dan music dan memanggil fungsi decoding untuk mendecode file tersebut.  pthread_equal digunakan untuk mengecek dan membandingkan threadID. Program akan mengecek file dengan akhiran .txt dan melakukan iterasi selama directory tidak bernilai NULL. Program akan membuka file menggunakan fungsi fopen dengan mode "r" untuk membaca file dan menggunakan fungsi fgets untuk membaca tiap baris dan menyimpannya ke dalam buffer. Program kemudian melakukan decoding pada buffer dengan fungsi decoding dan hasil decoding akan diappend pada file baru pada folder hasil sesuai jenisnya (quote atau music) kemudian ditutup. 
```
#define MUSIC_FOLDER "./music/"
#define QUOTE_FOLDER "./quote/"
#define HASIL_FOLDER "./hasil/"
#define HASIL_MUSIC_TXT "./hasil/music.txt"
#define HASIL_QUOTE_TXT "./hasil/quote.txt"

void *decode(void *arg)
{
  pthread_t id = pthread_self();

  if (pthread_equal(id, tid[0]))
  {
    DIR *directory;
    struct dirent *dir1;
    directory = opendir(MUSIC_FOLDER);

    while ((dir1 = readdir(directory)) != NULL)
    {
      if (strstr(dir1->d_name, ".txt") && strcmp(dir1->d_name, ".") && strcmp(dir1->d_name, ".."))
      {
        char temp_name_dir[FILENAME_MAX], loc[1024], buffer[1024];
        strcpy(temp_name_dir, MUSIC_FOLDER);
        strcat(temp_name_dir, dir1->d_name);
        FILE *file_music = fopen(temp_name_dir, "r");
        if (file_music != NULL)
          fgets(buffer, 1024, file_music);
        else
          printf("Task Failed :[Open File Failed]\n");
        fclose(file_music);
        decoding(buffer);
        FILE *file_hasil = fopen(HASIL_MUSIC_TXT, "a");
        fprintf(file_hasil, "%s\n", res);
        fclose(file_hasil);
        music_count++;
      }
    }
    closedir(directory);
  }
  else if (pthread_equal(id, tid[1]))
  {
    pthread_join(tid[0], NULL);

    DIR *directory;
    struct dirent *dir2;
    directory = opendir(QUOTE_FOLDER);
    while ((dir2 = readdir(directory)) != NULL)
    {
      if (strstr(dir2->d_name, ".txt") && strcmp(dir2->d_name, ".") && strcmp(dir2->d_name, ".."))
      {
        char temp_name_dir[FILENAME_MAX];
        char loc[1024], buffer[1024];
        strcpy(temp_name_dir, QUOTE_FOLDER);
        strcat(temp_name_dir, dir2->d_name);
        FILE *file_quote = fopen(temp_name_dir, "r");
        if (file_quote != NULL)
          fgets(buffer, 1024, file_quote);
        else
          printf("Task Failed :[Open File Failed]\n");
        fclose(file_quote);
        decoding(buffer);
        FILE *file_hasil = fopen(HASIL_QUOTE_TXT, "a");
        fprintf(file_hasil, "%s\n", res);
        fclose(file_hasil);
        quote_count++;
      }
    }
    closedir(directory);
  }
}

```

## C. Pemindahan File ke Folder Hasil
Pemindahan file ke folder hasil terjadi pada akhir fungsi decode yang ada di poin B.

```
#define HASIL_MUSIC_TXT "./hasil/music.txt"
#define HASIL_QUOTE_TXT "./hasil/quote.txt"

.
.
.
        FILE *file_hasil = fopen(HASIL_MUSIC_TXT, "a");
        fprintf(file_hasil, "%s\n", res);
        fclose(file_hasil);
.
.
.
        FILE *file_hasil = fopen(HASIL_QUOTE_TXT, "a");
        fprintf(file_hasil, "%s\n", res);
        fclose(file_hasil);
.
.
.
```

## D. Pengezipan dan password
Pengezipan dan password dilakukan dengan fungsi zip_file dengan password yang telah didefinisikan di awal. Di sini dipakai fungsi execv untuk mengeksekusi pengezipan dengan fungsi zip yang menggunakan argument -P untuk menambahkan password yang telah didefinisikan di awal, -r untuk melakukannya secara rekursiv, dan -q agar fungsi dijalankan dalam mode quiet sehingga tidak muncul informational messages atau comment prompts.

```
#define PASSWORD "mihinomenestcahyadi"
void zip_file()
{
  int status;
  child_process = fork();
  if (child_process == 0)
  {
    char *cmd[] = {"zip", "-P", PASSWORD, "-r", "./hasil.zip", "-q", "./hasil", NULL};
    execv("/bin/zip", cmd);
  }
  while ((wait(&status)) > 0)
    ;
}
```

## E. Unzip dan Rezip
Unzip dan rezip dilakukan menggunakan fungsi add_file_to_zip. Fungsi ini menjalankan dua thread. Thread pertama menjalankan fungsi pengunzipan menggunakan execv untuk mengeksekusi fungsi unzip dengan argumen -P untuk menambahkan password, dan -qq supaya program dijalankan dalam quiet mode sehingga tidak ada informational messages dan comment prompts. Thread kedua membuka file no.txt pada folder hasil dan melakukan append kata "No" di dalamnya. pthread_equal sekali lagi digunakan untuk membandingkan threadID dan menjalankan kedua thread pada waktu yang sama.

```
#define HASIL_NO_TXT "./hasil/no.txt"
void *add_file_to_zip(void *arg)
{
  pthread_t id = pthread_self();
  if (pthread_equal(id, tid[0]))
  {
    if (fork() == 0)
    {
      char *argv[] = {"unzip", "-P", PASSWORD, "-qq", HASIL, NULL};
      execv("/usr/bin/unzip", argv);
    }
  }
  else if (pthread_equal(id, tid[1]))
  {
    pthread_join(tid[0], NULL);

    FILE *file_no = fopen(HASIL_NO_TXT, "a");
    fprintf(file_no, "No");
    fclose(file_no);
  }
}
```

Setelah itu dilakukan pengezipan file dengan fungsi zip_file lagi seperti pada poin D.

```
void zip_file()
{
  int status;
  child_process = fork();
  if (child_process == 0)
  {
    char *cmd[] = {"zip", "-P", PASSWORD, "-r", "./hasil.zip", "-q", "./hasil", NULL};
    execv("/bin/zip", cmd);
  }
  while ((wait(&status)) > 0)
    ;
}
```

## Output

#### 1. Hasil download dan unzip file
![satu](img/1.png)

#### 2. Stat file music dan quote yang dibuat pada waktu yang sama
![dua](img/2.png)

#### 3. Isi file music
![lima](img/5.png)

#### 4. Isi file quote
![enam](img/6.png)

#### 5. Isi file decoding hasil
![tiga](img/3.png)

#### 6. File zip dengan password
![empat](img/4.png)

## Kendala
- Membuat fungsi yang tepat baik dari parameter dan logicnya sehingga tidak membuat code terlalu panjang dan lebih mudah dibaca
- File hasil tidak terzip dengan benar jika menggunakan WSL

# Soal 2
## Catatan
- Dilarang menggunakan system() dan execv(). Silahkan dikerjakan sepenuhnya dengan thread dan socket programming. 
- Untuk download dan upload silahkan menggunakan file teks dengan ekstensi dan isi bebas (yang ada isinya bukan touch saja dan tidak kosong)
- Untuk error handling jika tidak diminta di soal tidak perlu diberi.

## A. Register dan Login

Ketika Client dijalankan, Client akan mencoba untuk melakukan koneksi dengan server menggunakan socket. Terdapat beberapa kasus yang akan menyebabkan kegagalan dalam pengubungan server dan client yang akan mengembalikan pesan error seperti kegagalan dalam pembuatan socket yang akan mengembalikan "Failed to create socket", kegagalan dalam pencarian alamat address yang akan mengembalikan "Invalid address", dan kegagalan penyambungan koneksi yang akan mengembalikan "Connection failed". Jika koneksi berhasil dibuat, akan muncul pesan agar client mengetikkan sesuatu supaya server bisa mengkonfirmasi bahwa koneksi telah terjalin. 

```
struct sockaddr_in alamatServer;
  char *login = "login", *logout = "logout", *regist = "register";
  char temp, buffer[1100] = {0}, cmd[1100], username[1100], password[1100];
  if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
  {
    printf(Failed to create socket);
    return -1;
  }
  memset(&alamatServer, '0', sizeof(alamatServer));
  alamatServer.sin_family = AF_INET, alamatServer.sin_port = htons(port);
  if (inet_pton(AF_INET, "127.0.0.1", &alamatServer.sin_addr) <= 0)
  {
    printf("Invalid address\n");
    return -1;
  }
  if (connect(sock, (struct sockaddr *)&alamatServer, sizeof(alamatServer)) < 0)
  {
    printf("Connection failed\n");
    return -1;
  }
  readerConn = read(sock, buffer, 1100);
  printf("%s\n", buffer);
  while (!same("Connected", buffer))
  {
    printf("Enter anything to check whether you can connect to the server or not.\n");
    scanf("%s", cmd);
    send(sock, cmd, 1100, 0);
    readerConn = read(sock, buffer, 1100);
    printf("%s\n", buffer);
  }
  if (!same("Connected", buffer))
    return 0;

```
Setelah terjalin koneksi antara Client dan Server, akan muncul pilihan untuk Client melakukan login, register, atau exit. Fungsi scanf akan mengambil command dari client dan menjalankan fungsi selanjutnya sesuai dengan command yang diberikan oleh client.
```
  while (1)
  {
    printf("1. Login\n2. Register\n3. Exit\n");
    scanf("%s", cmd);
```
Jika client memasukkan command login, maka akan muncul pesan agar client memasukkan username dan password yang akan dikirimkan kepada server. Jika dikembalikan "LoginSuccess" dari server, client akan dapat masuk ke tahap berikutnya dan pesan bahwa login berhasil akan ditampilkan. Namun, jika tidak dikembalikan "LoginSuccess" dari server, akan ditampilkan pesan bahwa proses login telah gagal dan client dapat mencoba untuk memasukkan kembali informasinya.
```
  while (1)
  {
    if (same(cmd, login))
    {
      send(sock, login, strlen(login), 0);
      printf("Username: ");
      scanf("%c", &temp);
      scanf("%[^\n]", username);
      send(sock, username, 1100, 0);
      printf("Password: ");
      scanf("%c", &temp);
      scanf("%[^\n]", password);
      send(sock, password, 1100, 0);
      memset(buffer, 0, sizeof(buffer));
      readerConn = read(sock, buffer, 1100);
      if (same(buffer, "LoginSuccess"))
        printf("Login process is success! Welcome aboard! ^o^\n");
      else
        printf("Login process failed, please try again.\n");

```
Di sisi server, server akan membaca informasi login yang dikirimkan oleh klien dan membuka file user.txt dengan fopen mode "r" untuk membaca data user yang tersedia. Server akan membaca file pada user.txt dan melakukan compare terhadap informasi login yang telah diberikan oleh user untuk menentukan apakah user berhasil login atau tidak. Jika ditemukan informasi login yang valid, nilai check akan menjadi 1 dan server akan mengirimkan "LoginSuccess" sehingga client akan dapat melanjutkan ke tahap berikutnya.

```
  while (1)
  {
    readerConn = read(serverSocket, buffer, 1100);
    if (same(login, buffer))
    {
      readerConn = read(serverSocket, user.name, 1100);
      readerConn = read(serverSocket, user.password, 1100);
      fp3 = fopen("users.txt", "r");
      int check = 0;
      char *line = NULL;
      ssize_t len = 0;
      ssize_t fileReader;
      while ((fileReader = getline(&line, &len, fp3) != -1))
      {
        char userName[105], userPass[105];
        int i = 0, j = 0;
        while (line[i] != ':')
          userName[i] = line[i], i++;
        userName[i++] = '\0';
        while (line[i] != '\n')
          userPass[j] = line[i], j++, i++;
        userPass[j] = '\0';
        if (strcmp(user.name, userName) == 0 && strcmp(user.password, userPass) == 0)
        {
          check = 1, send(serverSocket, "LoginSuccess", strlen("LoginSuccess"), 0);
          break;
        }
      }
```
Namun, jika informasi login tidak sesuai dengan data yang dimiliki server, nilai check akan tetap 0 dan server akan mengirimkan "LoginFail" kepada client sehingga client harus memasukkan kembali informasi login.
```
      if (check == 0)
        printf("Authorization for Login is Failed\n"), send(serverSocket, "LoginFail", strlen("LoginFail"), 0);
      else
      {
        printf("Authorization for Login is Success\n");

```
Sedangkan jika client memasukkan command register, akan muncul pesan untuk memasukkan informasi registrasi yang terdiri dari username dan password. Client akan menggunakan fungsi scanf untuk membaca informasi tersebut dan mengirimkannya kepada server. Server akan mengembalikan "RegError" atau "Register Berhasil" tergantung kepada valid atau tidaknya informasi yang didapatkan. Jika diterima "RegError" dari server, akan ditampilkan pesan bahwa username sudah terambil atau password tidak sesuai ketentuan. Jika diterima "Register Berhasil" dari server, akan ditampilkan pesan bahwa proses registrasi sudah berhasil.
```
else if (same(cmd, regist))
    {
      send(sock, regist, strlen(regist), 0);
      memset(buffer, 0, sizeof(buffer));
      printf("Register\nUsername: ");
      scanf("%c", &temp);
      scanf("%[^\n]", username);
      send(sock, username, 1100, 0);
      printf("Password: ");
      scanf("%c", &temp);
      scanf("%[^\n]", password);
      send(sock, password, 1100, 0);
      readerConn = read(sock, buffer, 1100);
      if (same(buffer, "RegError"))
        printf("Register gagal, username sudah terambil atau password minimal terdiri dari 6 huruf, terdapat angka, terdapat huruf besar dan kecil.\n");
      if (same(buffer, "Register berhasil"))
        printf("Register berhasil\n");
      memset(buffer, 0, sizeof(buffer));
    }
```
Di sisi server, server akan membuka file user.txt dengan mode "a" dan "r" untuk melakukan append dan membaca file user.txt. Server akan menerima informasi registrasi kemudian mengecek dengan melakukan compare antara username yang ada di file.txt dan username yang diterima dari client. Jika tidak ditemukan perbedaan, maka username telah dipakai dan server akan melakukan break pada operasi dan mengembalikan "RegError". Jika pengecekan username berhasil dilewati tanpa break, server akan mengecek adanya minimal karakter, huruf kapital, huruf kecil, dan angka pada password dengan menggunakan nilai ASCII. Jika terdapat ketiganya, maka server akan melakukan append username dan password yang dimasukkan dalam file.txt dan mengembalikan "Register Berhasil" kepada client.
```
    else if (same(regist, buffer))
    {
      fp2 = fopen("users.txt", "a"), fp3 = fopen("users.txt", "r");
      readerConn = read(serverSocket, user.name, 1100),
      readerConn = read(serverSocket, user.password, 1100);
      int flag = 0;
      char *line = NULL;
      ssize_t len = 0;
      ssize_t fileReader;
      int check = 0, numUsers = 0;
      while ((fileReader = getline(&line, &len, fp3) != -1))
      {
        numUsers++;
        char userName[105], userPass[105];
        int i = 0;
        while (line[i] != ':')
          userName[i] = line[i], i++;
        userName[i] = '\0';
        i++;
        int j = 0;
        while (line[i] != '\n')
          userPass[j] = line[i], j++, i++;
        userPass[j] = '\0';
        if (strcmp(user.name, userName) == 0)
        {
          check = 0;
          break;
        }
        int kapital = 0, lower = 0, angka = 0, jumlah = 0;
        for (int i = 0; i < strlen(user.password); i++)
        {
          if (user.password[i] >= 48 && user.password[i] <= 57)
            angka = 1;
          if (user.password[i] >= 65 && user.password[i] <= 90)
            kapital = 1, jumlah++;
          if (user.password[i] >= 97 && user.password[i] <= 122)
            lower = 1, jumlah++;
        }
        if (!angka || !lower || !kapital || jumlah < 6)
          break;
        if (angka && lower && kapital && jumlah >= 6)
          check = 1;
      }
      if (check || numUsers == 0)
      {
        fprintf(fp2, "%s:%s\n", user.name, user.password);
        send(serverSocket, "Register berhasil", strlen("Register berhasil"), 0);
      }
      else
        send(serverSocket, "RegError", strlen("RegError"), 0);
      fclose(fp2);
    }
    else if (same("exit", buffer) || same("3", buffer))
    {
      close(serverSocket), total--;
      break;
    }
  }

```

## B. Database Problem

Jika server berhasil dijalankan, akan dieksekusi fungsi fopen mode "a" yang akan membuat file problems.tsv jika sebelumnya file tersebut belum ada.
```
int main(int argc, char const *argv[])
{
  struct sockaddr_in alamat;
  int serverSocket, serverFD, readerConn, opt = 1, jumlahKoneksi = 5, porter = 8080, addr_len = sizeof(alamat);
  if ((serverFD = socket(AF_INET, SOCK_STREAM, 0)) == 0)
    perror("socket failed"), exit(EXIT_FAILURE);
  if (setsockopt(serverFD, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt)))
    perror("setsockopt"), exit(EXIT_FAILURE);
  alamat.sin_family = AF_INET, alamat.sin_addr.s_addr = INADDR_ANY, alamat.sin_port = htons(porter);
  if (bind(serverFD, (struct sockaddr *)&alamat, sizeof(alamat)) < 0)
    perror("bind failed"), exit(EXIT_FAILURE);
  if (listen(serverFD, jumlahKoneksi) < 0)
    perror("listen"), exit(EXIT_FAILURE);
  mkdir("problems", 0777);
  FILE *fp = fopen("problems.tsv", "a");
```
User dapat menambahkan problem pada file ini dengan command add yang akan dijelaskan pada bagian C. File ini berisi judul problem dan username pembuat problem yang dipisahkan oleh \t sesuai perintah soal.
```
...
            fp = fopen("problems.tsv", "a");
            fprintf(fp, "%s\t%s\n", request.problem_title, user.name), fclose(fp);
...
```
## C. Add Problem

Setelah client berhasil melakukan login, client akan mendapatkan beberapa pilihan. Salah satunya adalah untuk melakukan add problem.
```
 if (same(buffer, "LoginSuccess"))
      {
        while (1)
        {
          printf("1. Logout\n2. Add\n3. Download [Problem Name]\n4. Submit [Problem Name] [Path Output]\n5. See\n");
          scanf("%s", cmd);
          if (same(cmd, logout))
          {
            send(sock, logout, 1100, 0);
            break;
          }
```
Jika client memasukkan command add, akan muncul pesan untuk memasukkan judul problem yang akan diambil menggunakan fungsi scanf dan dikirimkan kepada server. Akan muncul juga pesan untuk memasukkan filepath description.txt yang berisi deskripsi problem, filepath input.txt yang berisi input testcase untuk menyelesaikan problem, dan output.txt yang akan digunakan untuk melakukan pengecekan pada submission client. 
```
          else if (same(cmd, "add"))
          {
            send(sock, cmd, 1100, 0);
            char data[1100], problemName[1100];
            printf("Judul Problem: ");
            scanf("\n%[^\n]%*c", problemName);
            send(sock, problemName, 1100, 0);
            printf("Filepath description.txt: ");
            scanf("\n%[^\n]%*c", data);
            char outputFolder[1100], inputFolder[1100], descFolder[1100], outputPath[1100];
            strcpy(descFolder, data);
            removeStr(descFolder, "/description.txt");
            mkdir(descFolder, 0777);
            send(sock, data, 1100, 0);
            printf("Filepath input.txt: ");
            scanf("\n%[^\n]%*c", data);
            strcpy(inputFolder, data);
            removeStr(inputFolder, "/input.txt");
            mkdir(inputFolder, 0777);
            send(sock, data, 1100, 0);
            printf("Filepath output.txt: ");
            scanf("\n%[^\n]%*c", data);
            send(sock, data, 1100, 0);
            readerConn = read(sock, buffer, 1100);
            int numGacha = atoi(buffer);
            memset(buffer, 0, sizeof(buffer));
            strcpy(outputPath, data);
            FILE *fileManager;
            fileManager = fopen(outputPath, "a+");
            for (int i = 0; i < numGacha; i++)
              readerConn = read(sock, buffer, 1100),
              fprintf(fileManager, "%s", buffer),
              memset(buffer, 0, sizeof(buffer));
            fclose(fileManager);
            printf("Successfully adding problem %s to the server!\n", problemName);
            continue;
          }

```
Di sisi server, server akan membaca kiriman informasi dari client dan mengirimakn request untuk judul problem, path description, path input, dan path output. Server kemudian membuat direktori yang mengambil nama dari input judul problem yang dimasukkan oleh client dan menyimpan problem di dalamnya dengan strcpy dan strcat. Setelah itu, akan ditampilkan pesan "(username) is adding new problem called (judul) to the server".
```
          else if (same("add", buffer))
          {
            problem request;
            char basePath[1100], clientPath[1100], descPath[1100], inputPath[1100];
            char descServer[1100], inputServer[1100], outputServer[1100];
            readerConn = read(serverSocket, request.problem_title, 1100);
            printf("Judul problem: %s\n", request.problem_title);
            readerConn = read(serverSocket, request.pathDesc, 1100);
            printf("Filepath description.txt: %s\n", request.pathDesc);
            readerConn = read(serverSocket, request.pathInput, 1100);
            printf("Filepath input.txt: %s\n", request.pathInput);
            readerConn = read(serverSocket, request.pathOutput, 1100);
            printf("Filepath output.txt: %s\n", request.pathOutput);
            strcpy(basePath, "problems/");
            strcat(basePath, request.problem_title);
            mkdir(basePath, 0777);
            strcat(basePath, "/");
            strcpy(descServer, basePath);
            strcat(descServer, "description.txt");
            strcpy(inputServer, basePath);
            strcat(inputServer, "input.txt");
            strcpy(outputServer, basePath);
            strcat(outputServer, "output.txt");
            fp = fopen("problems.tsv", "a");
            fprintf(fp, "%s\t%s\n", request.problem_title, user.name), fclose(fp);
            printf("%s is adding new problem called %s to the server.\n", user.name, request.problem_title);
            continue;
          }

```

## D. See Problem

Setelah client berhasil melakukan login, client akan mendapatkan beberapa pilihan. Salah satunya adalah untuk melakukan see problem.
```
 if (same(buffer, "LoginSuccess"))
      {
        while (1)
        {
          printf("1. Logout\n2. Add\n3. Download [Problem Name]\n4. Submit [Problem Name] [Path Output]\n5. See\n");
          scanf("%s", cmd);
          if (same(cmd, logout))
          {
            send(sock, logout, 1100, 0);
            break;
          }
```
Jika user memasukkan command see, akan ditampilkan pesan berupa "Problems Available in Online Judge" dan client akan mengirimkan perintah "see" kepada server. Perintah ini akan membuat server mengembalikan daftar judul dan pembuat problem.
```
   else if (same(cmd, "see"))
          {
            printf("Problems Available in Online Judge:\n");
            send(sock, cmd, 1100, 0);
            memset(buffer, 0, sizeof(buffer));
            while (readerConn = read(sock, buffer, 1100))
            {
              if (same(buffer, "e"))
                break;
              printf("%s", buffer);
            }
            continue;

```
Di sisi server, jika server mendapatkan command "see", server akan membuka file problems.tsv dengan fungsi fopen mode "r" untuk membaca problem dan mengirimkan line demi line berisi judul problem dan pengarangnya kepada client sebelum menutup file tersebut dengan fclose.
```
          else if (same("see", buffer))
          {
            fp = fopen("problems.tsv", "r");
            if (!fp)
            {
              send(serverSocket, "e", sizeof("e"), 0);
              memset(buffer, 0, sizeof(buffer));
              continue;
            }
            char *line = NULL;
            ssize_t len = 0;
            ssize_t fileReader;
            while ((fileReader = getline(&line, &len, fp) != -1))
            {
              problem temp_problem;
              storeProblemTSV(&temp_problem, line);
              char message[1100];
              sprintf(message, "%s by %s", temp_problem.problem_title, temp_problem.author);
              send(serverSocket, message, 1100, 0);
            }
            send(serverSocket, "e", sizeof("e"), 0), fclose(fp);
            memset(buffer, 0, sizeof(buffer));
          }
        }
```
## E. Download Problem

Setelah client berhasil melakukan login, client akan mendapatkan beberapa pilihan. Salah satunya adalah untuk melakukan download problem.
```
 if (same(buffer, "LoginSuccess"))
      {
        while (1)
        {
          printf("1. Logout\n2. Add\n3. Download [Problem Name]\n4. Submit [Problem Name] [Path Output]\n5. See\n");
          scanf("%s", cmd);
          if (same(cmd, logout))
          {
            send(sock, logout, 1100, 0);
            break;
          }
```
Jika client memasukkan command download [judul-problem], client akan mengirimkan command tersebut menggunakan fungsi send kepada server. Client akan membuka file input target menggunakan fopen untuk membuat file tersebut jika belum ada dan mengeprint isi file yang dikirimkan oleh server ke dalam file input target sebelum menutupnya ketika tidak ada line yang dapat diprint lagi. Client kemudian membaca respon dari server dan akan menampilkan pesan bahwa file sedang didownload. Client akan memindahkan kiriman dari server ke dalam buffer dan menuliskannya ke dalam destinasi. Setelah itu, server akan menampilkan pesan bahwa file telah berhasil didownload.
```
          else if (same(cmd, "download"))
          {
            send(sock, cmd, 1100, 0);
            memset(buffer, 0, sizeof(buffer));
            scanf("%s", cmd);
            send(sock, cmd, 1100, 0);
            readerConn = read(sock, buffer, 1100);
            int numLine = atoi(buffer);
            char inputTarget[1100];
            readerConn = read(sock, inputTarget, 1100);
            for (int i = 0; i < numLine; i++)
            {
              FILE *fpClient = fopen(inputTarget, "r");
              fpClient = fopen(inputTarget, "a+");
              char fileContent[1100];
              readerConn = read(sock, fileContent, 1100);
              fprintf(fpClient, "%s\n", fileContent), fclose(fpClient);
            }
            memset(buffer, 0, sizeof(buffer));
            readerConn = read(sock, buffer, 1100);
            char fileReady[] = "File is ready to be downloaded.\n", descTarget[1100];
            if (same(buffer, fileReady))
            {
              memset(buffer, 0, sizeof(buffer));
              readerConn = read(sock, buffer, 1100);
              strcpy(descTarget, buffer);
              printf("Downloading description & input from problem %s..\n", cmd);
              int des_fd = open(descTarget, O_WRONLY | O_CREAT | O_EXCL, 0700);
              if (!des_fd)
                perror("can't open file"), exit(EXIT_FAILURE);
              int file_read_len;
              char buff[1100];
              while (1)
              {
                memset(buff, 0, 1100);
                file_read_len = read(sock, buff, 1100);
                write(des_fd, buff, file_read_len);
                break;
              }
              printf("Successfully downloading the files for problem %s to the desired client directory!\n", cmd);
            }
            memset(buffer, 0, sizeof(buffer));
            continue;

```
Di sisi server, server akan memeriksa apakah kiriman [judul-problem] dari client tersedia menggunakan strcpy dan strcat. Jika ditemukan, program akan menampilkan pesan bahwa file siap didownload dan jika tidak, akan ditampilkan pesan bahwa file tidak ditemukan. Program akan membuka file pada destinasi yang diminta oleh client dan menggunakan loop untuk mengirimkan isinya kepada client sebelum menutup file tersebut.
```
          else if (same("download", buffer))
          {
            readerConn = read(serverSocket, buffer, 1100);
            bool foundDesc = false, foundInput = false;
            char pathDownload[1100] = "problems/", descPath[1100], descTarget[1100];
            strcat(pathDownload, buffer);
            strcpy(descPath, pathDownload);
            strcat(descPath, "/description.txt");
            char inputPath[1100], inputTarget[1100], outputPath[1100], outputTarget[1100];
            strcpy(inputPath, pathDownload);
            strcat(inputPath, "/input.txt");
            strcpy(outputPath, pathDownload);
            strcat(outputPath, "/output.txt");
            char foundSuccess[] = "File is ready to be downloaded.\n", notFound[] = "No such file found.\n";
            fp = fopen(descPath, "r");
            FILE *source, *target;
            source = fopen(descPath, "r");
            char *line = NULL, *line2 = NULL, *line3 = NULL;
            ssize_t len = 0;
            ssize_t fileReader;
            int num = 0, fd, readLength;
            while ((fileReader = getline(&line, &len, source) != -1))
            {
              num++;
              if (num > 3)
              {
                char pathContent[105];
                int i = 0, j = 0;
                while (line[i] != ':')
                  i++;
                line[i] = '\0';
                i += 2;
                while (line[i] != '\n')
                  pathContent[j++] = line[i++];
                pathContent[j] = '\0';
                if (num == 4)
                  strcpy(descTarget, pathContent);
                if (num == 5)
                  strcpy(inputTarget, pathContent);
                if (num == 6)
                  strcpy(outputTarget, pathContent);
              }
            }
            int numLine = 0;
            FILE *fpSource = fopen(inputPath, "r");
            ssize_t len2 = 0;
            ssize_t fileReader2;
            bool checkSubmission = true;
            while ((fileReader2 = getline(&line2, &len2, fpSource) != -1))
              numLine++;
            fclose(fpSource);
            fpSource = fopen(inputPath, "r");
            char buf[1100];
            sprintf(buf, "%d", numLine);
            send(serverSocket, buf, 1100, 0);
            send(serverSocket, inputTarget, 1100, 0);
            ssize_t len3 = 0;
            ssize_t fileReader3;
            while ((fileReader3 = getline(&line3, &len3, fpSource) != -1))
            {
              char outputSource[105];
              int i = 0;
              while (line3[i] != '\n')
                outputSource[i] = line3[i], i++;
              outputSource[i] = '\0';
              send(serverSocket, outputSource, 1100, 0);
            }
            fclose(fpSource);
            if (fp)
              foundDesc = true;
            if (!foundDesc)
              send(serverSocket, notFound, 1100, 0);
            else
            {
              send(serverSocket, foundSuccess, 1100, 0);
              send(serverSocket, descTarget, 1100, 0);
              fd = open(descPath, O_RDONLY);
              if (!fd)
                perror("Fail to open"), exit(EXIT_FAILURE);
              while (1)
              {
                memset(descPath, 0x00, 1100);
                readLength = read(fd, descPath, 1100);
                if (readLength == 0)
                  break;
                else
                  send(serverSocket, descPath, readLength, 0);
              }
              close(fd);
            }
            fclose(fp);
          }
```
## F. Submit Problem
Jika client memasukkan command submit [judul-problem] [path-file-output], client akan mengirim command tersebut kepada server kemudian memasukkan [judul-problem] dan [path-file-output] untuk dikirimkan juga kepada server. Client kemudian akan mencoba untuk membuka file dan membacanya. Jika file tidak tersedia, akan dikembalikan pesan "Output file is not exist!" dan break akan terjadi. Jika file tersedia, program akan masuk ke dalam loop untuk mengambil line demi line dari output submission menggunakan fungsi getline untuk dikirimkan kepada server. Setelah tidak ada line untuk diambil lagi, program akan membaca perintah yang dikembalikan oleh server dan menampilkan hasil dari submission.
```
          else if (same(cmd, "submit"))
          {
            send(sock, cmd, 1100, 0);
            memset(buffer, 0, sizeof(buffer));
            char namaProblem[1100], filePath[1100];
            scanf("%s %s", namaProblem, filePath);
            send(sock, namaProblem, 1100, 0);
            FILE *fpC = fopen(filePath, "r");
            if (!fpC)
            {
              printf("Output file is not exist!\n");
              break;
            }
            char *line = NULL;
            ssize_t len = 0;
            ssize_t fileReader;
            bool checkSubmission = true;
            while ((fileReader = getline(&line, &len, fpC) != -1))
            {
              char outputSubmission[105];
              int i = 0, j = 0;
              while (line[i] != ':')
                i++;
              line[i++] = '\0';
              while (line[i] != '\n')
                outputSubmission[j++] = line[i++];
              outputSubmission[j] = '\0';
              send(sock, outputSubmission, 105, 0);
            }
            memset(buffer, 0, sizeof(buffer));
            readerConn = read(sock, buffer, 1100);
            printf("Your output.txt file for problem %s is submitted!\nResult: %s\n", namaProblem, buffer);
          }

```
Di sisi server, server akan membaca informasi [judul-problem] [path-file-output] dari soal dan membuka file yang berada di destinasi path yang diberikan. Server kemudian mengambil line demi line menggunakan loop untuk membandingkan antara isi dari output yang disubmit oleh client dan output yang tersedia di database. Jika yang disubmit tidak sama dengan yang ada di database server, akan dikembalikan false kepada client. 
```
          else if (same("submit", buffer))
          {
            char problemName[1100], pathSource[1100] = "problems/";
            readerConn = read(serverSocket, problemName, 1100);
            strcat(pathSource, problemName);
            strcat(pathSource, "/output.txt");
            FILE *fpSource = fopen(pathSource, "r");
            char *line = NULL;
            ssize_t len = 0;
            ssize_t fileReader;
            bool checkSubmission = true;
            while (fileReader = getline(&line, &len, fpSource) != -1)
            {
              char outputSource[105], outputSubmission[105];
              int i = 0, j = 0;
              while (line[i] != ':')
                i++;
              line[i++] = '\0';
              while (line[i] != '\n')
                outputSource[j++] = line[i++];
              outputSource[j] = '\0';
              readerConn = read(serverSocket, outputSubmission, 105);
              int source = atoi(outputSource), submit = atoi(outputSubmission);
              printf("[%s] source: %d dan submission: %d\n", submit == source ? "AC" : "WA", source, submit);
              if (submit != source)
                checkSubmission = false;
            }
            printf("%s is %s on problem %s!\n", user.name, checkSubmission ? "accepted (AC)" : "having a wrong answer (WA)", problemName);
            send(serverSocket, checkSubmission ? "AC\nAccepted in Every Testcases\n" : "WA\nWA in the First Testcase\n", 1100, 0);
          }
```
## G. Multiple Connection

Program menangani multiple connection dengan mengecek berapa banyak client yang terkoneksi. Jika terdapat tepat satu client, server akan mengirimkan pesan connected kepada client, sedangkan jika tidak maka server akan mengirimkan pesan failed. Selama terbaca bahwa total client lebih dari satu, server akan mengecek banyak client yang terhubung. Jika setelah itu jumlah client adalah satu, maka server akan mengirimkan pesan connected. Namun, jika tidak, server akan mengirimkan pesan failed. Failed akan menampilkan pada client yang gagal terhubung bahwa ada client yang sedang terkoneksi dan untuk menunggu koneksi diakhiri.
```
  char buffer[1100] = {0}, connected[1100] = "Connected", failed[1100] = "Other people are connected, please wait for them ^o^";
  int readerConn, serverSocket = *(int *)tmp;
  if (total == 1)
    send(serverSocket, connected, 1100, 0);
  else
    send(serverSocket, failed, 1100, 0);
  while (total > 1)
  {
    readerConn = read(serverSocket, buffer, 1100);
    if (total == 1)
      send(serverSocket, connected, 1100, 0);
    else
      send(serverSocket, failed, 1100, 0);
  }

```

## Output

#### 1. Register dan Login 
![satu](img/2.1.png)

#### 2. Database Problem
![dua](img/2.2.png)

#### 3. Add Problem
![lima](img/2.3.png)

#### 4. See Problem
![enam](img/2.4.png)

#### 5. Download Problem
![tiga](img/2.5.png)

#### 6. Submit Problem
![asd](img/2.6.png)

#### 7. Multiple Connections
![asd](img/2.7.png)

## Kendala
- Membuat fungsi yang tepat baik dari parameter dan logicnya sehingga tidak membuat code terlalu panjang dan lebih mudah dibaca

# Soal 3
## Catatan
- Kategori folder tidak dibuat secara manual, harus melalui program C
- Program ini tidak case sensitive. Contoh: JPG dan jpg adalah sama
- Jika ekstensi lebih dari satu (contoh “.tar.gz”) maka akan masuk ke folder dengan titik terdepan (contoh “tar.gz”)
- Dilarang juga menggunakan fork, exec dan system(), kecuali untuk bagian zip pada soal d

## A. Extract Zip
Langkah awal yaitu mengekstrak file hartakarun.zip yang telah disediakan di soal. Dengan menggunakan fungsi popen() dengan argumen pertama yaitu value, dimana variabel value berisi path ke file .zip-nya dan argumen kedua yaitu r untuk membaca filenya. Kemudian menggunakan fungsi fread pada variabel chars_read akan mem-print proses ekstraksi file di dalam arsip ke terminal. Fungsi yang bertanggung jawab dalam hal ini adalah fungsi extract().

```
void extract(char *value) 
{
    FILE *file;
    char buffer[BUFSIZ+1];
    int chars_read;

    memset(buffer,'\0',sizeof(buffer));

    file = popen(value ,"r");

    if(file != NULL){
        chars_read = fread(buffer, sizeof(char), BUFSIZ, file);
        if(chars_read > 0) printf("%s\n",buffer);
        pclose(file);
    }
}
```

## B. Mengkategorikan File dalam Folder
Untuk mengkategorikan file, program akan memanggil fungsi sortFile() untuk menjalankan tugas tersebut. Berikut tampilan fungsi sortFile():

```
void sortFile(char *from)
{
    struct_path s_path;
    struct dirent *dp;
    DIR *dir;

    flag = 1;
    int index = 0;
    strcpy(s_path.cwd, "/home/aga/shift3/hartakarun");

    dir = opendir(from);

    while((dp = readdir(dir)) != NULL){
        if(strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0){
            char fileName[10000];
            sprintf(fileName, "%s/%s", from, dp->d_name);
            strcpy(s_path.from, fileName);

            pthread_create(&tid[index], NULL, move, (void *)&s_path);
            sleep(1);
            index++;
        }
    }

    if(!flag){
        printf("Maaf kawan, lupakan saja :)\n");
    }else {
        printf("Kamu bisa lanjut, tapi jangan berharap lebih :)\n");
    }
}
```

Pada fungsi ini program akan membaca directory hasil ekstrak file .zip dan mengecek satu-persatu filenya. Informasi mengenai directory asal dan nama filenya disimpan ke dalam variabel fileName. Tugas ini dilakukan pada potongan code di bawah ini:

```
    dir = opendir(from);

    while((dp = readdir(dir)) != NULL){
        if(strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0){
            char fileName[10000];
            sprintf(fileName, "%s/%s", from, dp->d_name);
            strcpy(s_path.from, fileName);
```

## C. Mengkategorikan File Menggunakan Thread
Pembuatan thread dilakukan pada fungsi sortFile() yang juga memanggil fungsi move().

```
pthread_create(&tid[index], NULL, move, (void *)&s_path);
```

Pada fungsi move() program akan meamnggil fungsi moveFile() kemudian keluar dari thread.

```
{
    struct_path s_paths = *(struct_path*) s_path;
    moveFile(s_paths.from, s_paths.cwd);
    pthread_exit(0);
}
```

Berikut adalah tampilan fungsi moveFile():

```
void moveFile(char *p_from, char *p_cwd)
{
    char file_name[300], file_ext[30], temp[300];
    char *buffer;

    strcpy(temp, p_from);
    buffer = strtok(temp, "/");

    while(buffer != NULL){
        if(buffer != NULL) strcpy(file_name, buffer);
        buffer = strtok(NULL, "/");
    }

    strcpy(temp, file_name);
    
    buffer = strtok(temp, ".");
    buffer = strtok(NULL, "");

    if(file_name[0] == '.'){
        strcpy(file_ext, "Hidden");
    }else if(buffer != NULL){
        strcpy(file_ext, buffer);
    }else{
        strcpy(file_ext, "Unknown");
    }

    toLower(file_ext);

    // new dest
    strcat(p_cwd, "/");
    strcat(p_cwd, file_ext);
    strcat(p_cwd, "/");
    strcat(p_cwd, file_name);

    // information
    printf("from %s: \nfile_name: %s\nfile_ext: %s\ndest: %s\n\n", p_from, file_name, file_ext, p_cwd);

    // create dir and move file
    createDir(file_ext);
    rename(p_from, p_cwd);
}
```

Langkah awal pada fungsi ini yaitu nama file ke dalam variabel file_name dengan bantuan variabel buffer yang sebelumnya akan diisi nama file dengan bantuan fungsi strtok untuk mencari delimiter "/".

```
    buffer = strtok(temp, "/");

    while(buffer != NULL){
        if(buffer != NULL) strcpy(file_name, buffer);
        buffer = strtok(NULL, "/");
    }
```

Selanjutnya menentukan eksintensi file dengan bantuan variabel buffer yang akan berisi format file setelah diinisiasi hasil dari strtok dengan delimiter ".". Terdapat pengecualian pada pengkategorian file ini yaitu apabila variabel buffer berisi NULL yang artinya eksintensi file akan dikategorikan hidden (apabila file diawali karakter ".") dan unknown (apabila tidak memenuhi kedua kondisi).

```
    strcpy(temp, file_name);
    
    buffer = strtok(temp, ".");
    buffer = strtok(NULL, "");

    if(file_name[0] == '.'){
        strcpy(file_ext, "Hidden");
    }else if(buffer != NULL){
        strcpy(file_ext, buffer);
    }else{
        strcpy(file_ext, "Unknown");
    }
```

Selanjutnya adalah membuat destinasi dari file dengan menambahkan eksistensi dan nama file ke dalam path directory lama menggunakan strcat().

```
    strcat(p_cwd, "/");
    strcat(p_cwd, file_ext);
    strcat(p_cwd, "/");
    strcat(p_cwd, file_name);
```

Kemudian langkah terakhir adalh menampilkan informasi file dan membuat directory berdasarkan eksistensi filenya.

```
    // information
    printf("from %s: \nfile_name: %s\nfile_ext: %s\ndest: %s\n\n", p_from, file_name, file_ext, p_cwd);

    // create dir and move file
    createDir(file_ext);
    rename(p_from, p_cwd);
```

## D. Client-Server dan Pengezipan Harta Karun

## E. Send Harta Karun

## Output

#### 1. Extract Zip
![satu](img/3.1.png)

#### 2. Informasi File
![dua](img/3.2.png)

#### 3. Hasil Pengkategorian File Berdasarkan Eksistensi
![tiga](img/3.3.png)

#### 4. Isi Folder Pengecualian
##### a. hidden
![empat](img/3.4.png)
##### b. unknown
![empat](img/3.5.png)

## Kendala
- Menyelesaikan file client-server

