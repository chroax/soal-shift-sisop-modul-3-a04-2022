
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <pthread.h>
#include <memory.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <errno.h>
#include <syslog.h>
#include <pwd.h>
#include <time.h>

#define MUSIC_URL "https://drive.google.com/u/0/uc?id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1&export=download"
#define QUOTE_URL "https://drive.google.com/u/0/uc?id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt&export=download"
#define PATH "~/Documents/praktikum-sisop/praktikum-3/soal1"
#define MUSIC "music.zip"
#define QUOTE "quote.zip"
#define HASIL "hasil.zip"
#define MUSIC_FOLDER "./music/"
#define QUOTE_FOLDER "./quote/"
#define HASIL_FOLDER "./hasil/"
#define HASIL_MUSIC_TXT "./hasil/music.txt"
#define HASIL_QUOTE_TXT "./hasil/quote.txt"
#define HASIL_NO_TXT "./hasil/no.txt"
#define PASSWORD "mihinomenestcahyadi"

char base64[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'};
char *decoding_table;
char music[256][256];
char quote[256][256];
char res[256];
int music_count = 0, quote_count = 0;
pthread_t tid[10];
pid_t child_process;

void *download_file(void *arg);
void decoding(char *txt);
void *decode(void *arg);
void zip_file();
void *add_file_to_zip(void *arg);

int main(int argc, char const *argv[])
{
  int err;
  for (int i = 0; i < 2; i++)
    err = pthread_create(&(tid[i]), NULL, &download_file, NULL);

  pthread_join(tid[1], NULL);

  for (int i = 0; i < 2; i++)
    err = pthread_create(&(tid[i]), NULL, &decode, NULL);

  pthread_join(tid[1], NULL);

  zip_file();

  for (int i = 0; i < 2; i++)
    err = pthread_create(&(tid[i]), NULL, &add_file_to_zip, NULL);

  pthread_join(tid[1], NULL);

  zip_file();

  return 0;
}

void *download_file(void *arg)
{
  pthread_t id = pthread_self();
  if (pthread_equal(id, tid[0]))
  {
    int status;
    if (fork() == 0)
    {
      char *argv[] = {"mkdir", "-p", "music", NULL};
      execv("/usr/bin/mkdir", argv);
    }
    else
    {
      while ((wait(&status)) > 0)
        ;
      int status2;

      if (fork() == 0)
      {
        char *argv[] = {"wget", "-q", "--no-check-certificate", MUSIC_URL, "-O", MUSIC, NULL};
        execv("/usr/bin/wget", argv);
      }

      while ((wait(&status2)) > 0)
        ;

      if (fork() == 0)
      {
        char *argv[] = {"unzip", "-qq", MUSIC, "-d", "music", NULL};
        execv("/usr/bin/unzip", argv);
      }

      while ((wait(&status2)) > 0)
        ;
    }
  }
  else if (pthread_equal(id, tid[1]))
  {
    pthread_join(tid[0], NULL);

    int status;
    if (fork() == 0)
    {
      char *argv[] = {"mkdir", "-p", "quote", NULL};
      execv("/usr/bin/mkdir", argv);
    }
    else
    {
      while ((wait(&status)) > 0)
        ;
      int status2;
      if (fork() == 0)
      {
        char *argv[] = {"wget", "-q", "--no-check-certificate", QUOTE_URL, "-O", QUOTE, NULL};
        execv("/usr/bin/wget", argv);
      }

      while ((wait(&status2)) > 0)
        ;

      if (fork() == 0)
      {
        char *argv[] = {"unzip", "-qq", QUOTE, "-d", "quote", NULL};
        execv("/usr/bin/unzip", argv);
      }

      while ((wait(&status2)) > 0)
        ;

      if (fork() == 0)
      {
        char *argv[] = {"mkdir", "-p", "hasil", NULL};
        execv("/usr/bin/mkdir", argv);
      }

      while ((wait(&status2)) > 0)
        ;
    }
  }
}

void decoding(char *txt)
{
  char buff[4], result[100];
  int i = 0, index = 0, count = 0, j;

  while (txt[i] != '\0')
  {
    for (j = 0; j < 64 && base64[j] != txt[i]; j++)
      ;
    buff[count++] = j;
    if (count == 4)
    {
      result[index++] = (buff[0] << 2) + (buff[1] >> 4);
      if (buff[2] != 64)
        result[index++] = (buff[1] << 4) + (buff[2] >> 2);
      if (buff[3] != 64)
        result[index++] = (buff[2] << 6) + buff[3];
      count = 0;
    }
    i++;
  }

  result[index] = '\0';
  strcpy(res, result);
}

void *decode(void *arg)
{
  pthread_t id = pthread_self();

  if (pthread_equal(id, tid[0]))
  {
    DIR *directory;
    struct dirent *dir1;
    directory = opendir(MUSIC_FOLDER);

    while ((dir1 = readdir(directory)) != NULL)
    {
      if (strstr(dir1->d_name, ".txt") && strcmp(dir1->d_name, ".") && strcmp(dir1->d_name, ".."))
      {
        char temp_name_dir[FILENAME_MAX], loc[1024], buffer[1024];
        strcpy(temp_name_dir, MUSIC_FOLDER);
        strcat(temp_name_dir, dir1->d_name);
        FILE *file_music = fopen(temp_name_dir, "r");
        if (file_music != NULL)
          fgets(buffer, 1024, file_music);
        else
          printf("Task Failed :[Open File Failed]\n");
        fclose(file_music);
        decoding(buffer);
        FILE *file_hasil = fopen(HASIL_MUSIC_TXT, "a");
        fprintf(file_hasil, "%s\n", res);
        fclose(file_hasil);
        music_count++;
      }
    }
    closedir(directory);
  }
  else if (pthread_equal(id, tid[1]))
  {
    pthread_join(tid[0], NULL);

    DIR *directory;
    struct dirent *dir2;
    directory = opendir(QUOTE_FOLDER);
    while ((dir2 = readdir(directory)) != NULL)
    {
      if (strstr(dir2->d_name, ".txt") && strcmp(dir2->d_name, ".") && strcmp(dir2->d_name, ".."))
      {
        char temp_name_dir[FILENAME_MAX];
        char loc[1024], buffer[1024];
        strcpy(temp_name_dir, QUOTE_FOLDER);
        strcat(temp_name_dir, dir2->d_name);
        FILE *file_quote = fopen(temp_name_dir, "r");
        if (file_quote != NULL)
          fgets(buffer, 1024, file_quote);
        else
          printf("Task Failed :[Open File Failed]\n");
        fclose(file_quote);
        decoding(buffer);
        FILE *file_hasil = fopen(HASIL_QUOTE_TXT, "a");
        fprintf(file_hasil, "%s\n", res);
        fclose(file_hasil);
        quote_count++;
      }
    }
    closedir(directory);
  }
}

void *add_file_to_zip(void *arg)
{
  pthread_t id = pthread_self();
  if (pthread_equal(id, tid[0]))
  {
    if (fork() == 0)
    {
      char *argv[] = {"unzip", "-P", PASSWORD, "-qq", HASIL, NULL};
      execv("/usr/bin/unzip", argv);
    }
  }
  else if (pthread_equal(id, tid[1]))
  {
    pthread_join(tid[0], NULL);

    FILE *file_no = fopen(HASIL_NO_TXT, "a");
    fprintf(file_no, "No");
    fclose(file_no);
  }
}

void zip_file()
{
  int status;
  child_process = fork();
  if (child_process == 0)
  {
    char *cmd[] = {"zip", "-P", PASSWORD, "-r", "./hasil.zip", "-q", "./hasil", NULL};
    execv("/bin/zip", cmd);
  }
  while ((wait(&status)) > 0)
    ;
}
